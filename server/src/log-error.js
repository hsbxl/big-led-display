process.on("uncaughtException", (err) => {
  console.error("There was an uncaught error", err);
  process.exit(1); // Exits the process with an 'error' code (1)
});
