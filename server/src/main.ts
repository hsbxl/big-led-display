import "./pam.ts";

import fastifyWebsocket from "@fastify/websocket";
import Fastify from "fastify";
import { internalIpV4 } from "internal-ip";
import { createClient, EventType, MsgType, RoomEvent } from "matrix-js-sdk";
import { logger } from "matrix-js-sdk/lib/logger.js";
import fs from "node:fs/promises";
import { WebSocket } from "ws";

import {
  MATRIX_ACCESS_TOKEN,
  MATRIX_CHATROOM,
  MATRIX_SERVER,
  MATRIX_USER_ID,
  VERSION,
} from "./config.js";
import flashFirmware from "./flash-firmware.js";
import activeDevicesStream from "./pam.js";

const client = createClient({
  baseUrl: MATRIX_SERVER,
  userId: MATRIX_USER_ID,
  accessToken: MATRIX_ACCESS_TOKEN,
});
logger.setLevel(logger.levels.ERROR, false);

client.setGuest(true);

await client.setDisplayName("LED Display");

const mainRoom = await client.joinRoom(MATRIX_CHATROOM);

const { joined_rooms: joinedRooms } = await client.getJoinedRooms();
for (let roomID of joinedRooms) {
  if (roomID !== mainRoom.roomId) {
    await client.leave(roomID);
  }
}

await client.startClient({
  initialSyncLimit: 1,
});

console.log("Room joined");

client.on(RoomEvent.Timeline, function (event, room, toStartOfTimeline) {
  if (event.getType() !== EventType.RoomMessage) {
    return;
  }

  const content = event.getContent();
  if (content.msgtype !== MsgType.Text) {
    return;
  }

  const sender = event.getSender();
  const text: string = content.body;
  const message = sender + ": " + text;
  console.log("Matrix chat message " + message);
  queueMessage(text);
});

const server = Fastify();
server.register(fastifyWebsocket);

let display: {
  socket: WebSocket;
  messageQueue: string[];
  usedFramebuffers: number;
} | null = null;

const OTA_FILE = await (async () => {
  try {
    return await fs.readFile("firmware.bin");
  } catch (e) {
    if (e instanceof Error && "code" in e && e.code === "ENOENT") {
      return null;
    }
    console.error("OTA FILE ERROR");
    throw e;
  }
})();

if (OTA_FILE) {
  console.log("Firmware loaded");
} else {
  console.log("Firmware missing");
}

let spaceOpened = false;
activeDevicesStream.subscribe((val) => {
  spaceOpened = val;
  console.log({ val });
  queueMessage("Welcome to HSBXL!");
  queueMessage("Welcome to HSBXL!");
});

server.register(async (server) => {
  server.get("/display", { websocket: true }, async (connection, req) => {
    const clientVersion = req.headers["version"];
    console.log("Display connected");
    console.log("IP: " + req.ip);
    if (clientVersion) {
      console.log("Firmware version: " + clientVersion);
    }
    display?.socket.terminate();
    display = {
      socket: connection.socket,
      messageQueue: [],
      usedFramebuffers: 3,
    };
    connection.socket.on("close", () => {
      if (display?.socket === connection.socket) {
        display = null;
        console.log("Display disconnected");
      } else {
        console.log("Terminate stale socket");
      }
    });

    connection.socket.on("message", (data, isBinary) => {
      if (isBinary && display?.socket == connection.socket) {
        console.log("Framebuffer freed");
        if (display) {
          display.usedFramebuffers -= 1;
          if (display.usedFramebuffers < 0) {
            console.warn("Invalid server data");
            display.socket.terminate();
          } else {
            attemptSend(display);
          }
        }
      }
      console.log("DisplayLog", data.toString().trim());
    });

    if (clientVersion !== VERSION && OTA_FILE) {
      if (clientVersion === "dev") {
        queueMessage("Hello dev!");
        await new Promise((resolve) => {
          setTimeout(resolve, 60 * 60 * 1000);
        }); // wait an hour before flashing over a dev version
      }
      flashFirmware(connection.socket, OTA_FILE);
    } else {
      queueMessage("Welcome to HSBXL!");
    }
  });
});
server.get("/", async (request, reply) => {
  return { messageURL: "/message/hello-world", method: "GET" };
});

const FRAMEBUFFER_COUNT = 4;
const attemptSend = (display_: Exclude<typeof display, null>) => {
  while (
    display_.usedFramebuffers < FRAMEBUFFER_COUNT &&
    display_.messageQueue.length > 0
  ) {
    const message = display_.messageQueue.shift();
    if (message === undefined) {
      throw new Error("Missing message");
    }
    console.log("send message", JSON.stringify(message));
    display_.socket.send(spaceOpened ? message : "");
    display_.usedFramebuffers += 1;
  }
};
const queueMessage = (message: string) => {
  console.log("queue message", JSON.stringify(message));
  const LINE_CHARS = 25;

  const chunks = Array.from(
    { length: Math.ceil(message.length / LINE_CHARS) },
    (_, idx) => message.substring(idx * LINE_CHARS, (idx + 1) * LINE_CHARS),
  );

  if (!display) {
    return;
  }
  display.messageQueue.push(...chunks);
  attemptSend(display);
};
server.get("/message/:message", async (req, res) => {
  if (
    typeof req.params !== "object" ||
    !req.params ||
    !("message" in req.params) ||
    typeof req.params.message !== "string"
  ) {
    res.statusCode = 401;
    return "Missing message";
  }
  queueMessage(req.params.message);

  return {
    display: display ? "connected" : "disconnected",
  };
});

const PORT = 3000;
await server.listen({
  host: "::",
  port: PORT,
});
console.log("Version " + VERSION);
const ip = await internalIpV4();
if (ip) {
  console.log(
    `Server is running at http://` + (await internalIpV4()) + ":" + PORT,
  );
} else {
  console.log("Server is running on port " + PORT);
}
