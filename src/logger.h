#ifndef LOGGER_H
#define LOGGER_H

#include <Arduino.h>
#include <WebSocketsClient.h>

// #define LOG_TO_WEBSOCKET // Uncomment to enable logging to WebSocket as well

extern WebSocketsClient webSocket;

class Logger {
public:
  static void print(const char *message) {
    Serial.print(message);
#ifdef LOG_TO_WEBSOCKET
    if (webSocket.isConnected()) {
      webSocket.sendTXT(message);
    }
#endif
  }
  static void print(const String &message) { Logger::print(message.c_str()); }

  static void println(const char *message) { Logger::println(String(message)); }

  static void println(const String &message) { Logger::print(message + "\n"); }

  static void printf(const char *format, ...) {
    char buf[512];
    va_list args;
    va_start(args, format);
    vsnprintf(buf, sizeof(buf), format, args);
    va_end(args);
    Logger::print(String(buf));
  }
};

#endif // LOGGER_H
